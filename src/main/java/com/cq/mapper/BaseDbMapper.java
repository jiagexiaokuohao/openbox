package com.cq.mapper;

import java.util.List;
import java.util.Map;


/**
 * @author zhouqi
 */
public interface BaseDbMapper<T> {
    /**
     * 批量创建
     *
     * @param t
     */
    void create(T t);

    /**
     * 批量创建
     *
     * @param list
     */
    void createBatch(List<T> list);

    /**
     * 通过id 查询
     *
     * @param id
     * @return
     */
    T findById(Integer id);

    /**
     * 通过id 查询
     *
     * @param id
     * @return
     */
    T findByStrId(String id);


    /**
     * 更新对象
     *
     * @param t
     * @return
     */
    int update(T t);

    /**
     * 通过id 删除
     *
     * @param id
     */
    void deleteById(Long id);

    /**
     * 通过id 删除
     *
     * @param id
     */
    void deleteByStrId(String id);

    /**
     * 查询所有数据
     *
     * @return
     */
    List<Map<String, Object>> findAllMap();


    /**
     * 查询所有
     *
     * @return
     */
    List<T> findAll();
}
