package com.cq.mapper;

import com.cq.model.SysUser;

/**
 * @author zhouqi
 */
public interface SysUserMapper extends BaseDbMapper<SysUser> {

    /**
     * 123
     *
     * @param username 1
     * @return sysUser
     */
    SysUser findByUsername(String username);

}
