package com.cq.utils;

import com.cq.common.config.constant.Constants;
import com.cq.model.SysUser;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;

/**
 * @author zhouqi
 */
public class UserUtils {

    public static SysUser getUser() {
        return (SysUser) getSession().getAttribute(Constants.USER_SESSION_NAME);
    }

    public static void setUserSession(SysUser sysUser) {
        getSession().setAttribute(Constants.USER_SESSION_NAME, sysUser);
    }

    public static Session getSession() {
        Subject currentUser = SecurityUtils.getSubject();
        return currentUser.getSession();
    }
}
