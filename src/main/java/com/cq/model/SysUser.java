package com.cq.model;

import lombok.Data;

import java.util.Date;

/**
 * @author zhouqi
 */
@Data
public class SysUser {

    private Integer id;

    private String username;

    private String password;

    private String name;

    private int role;

    private Date createTime;
}
