package com.cq.model;

import lombok.Data;

import java.util.Date;

/**
 * @author zhouqi
 */
@Data
public class SysFile {

    private Integer id;
    private String ip;
    /**
     * 原名
     */
    private String originName;
    /**
     * 文件类型 .png   .xls
     */
    private String suffixes;
    private Date createTime;
    /**
     * 上传路径
     */
    private String path;

}
