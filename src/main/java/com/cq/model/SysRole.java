package com.cq.model;

import lombok.Data;

/**
 * @author zhouqi
 */
@Data
public class SysRole {
    private Integer id;

    private String name;

    private String code;
}
