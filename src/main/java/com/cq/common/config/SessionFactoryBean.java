package com.cq.common.config;

import lombok.Data;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import javax.sql.DataSource;

/**
 * @author txw
 */
@Data
class SessionFactoryBean {
    private String model;
    private String classPath;

    SessionFactoryBean(String model, String classPath) {
        this.model = model;
        this.classPath = classPath;
    }

    SqlSessionFactory newInstance(DataSource dataSource) throws Exception {
        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setTypeAliasesPackage(model);
        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        factoryBean.setMapperLocations(resolver.getResources(classPath));
        return factoryBean.getObject();
    }
}
