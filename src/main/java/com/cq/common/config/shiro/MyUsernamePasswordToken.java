package com.cq.common.config.shiro;

import org.apache.shiro.authc.UsernamePasswordToken;

/**
 * @author zhouqi
 */
public class MyUsernamePasswordToken extends UsernamePasswordToken {

    public MyUsernamePasswordToken(String username, String password) {
        super(username, password);
    }
}
