package com.cq.common.config;

import lombok.Data;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

/**
 * @author admin
 */
@Configuration
@Data
@ConfigurationProperties(prefix = "spring.datasource")
@MapperScan(basePackages = {"com.cq.mapper"}, sqlSessionFactoryRef = "systemRepoSqlSessionFactory")
public class SysRepoConfig {

    private String jdbcUrl;

    private String username;

    private String password;

    private String driverClassName;

    private Integer maximumPoolSize;

    private Integer minimumIdle;

    private String model = "com.cq.model";

    private String classPath = "classpath:/mapper/*.xml";


    @Primary
    @Bean(name = "sysRepoDataSource")
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource dataSource() {
        return new HikariConfig(jdbcUrl, username, driverClassName, password, maximumPoolSize, minimumIdle).buildHikari();
    }

    @Primary
    @Bean(name = "sysRepoTransactionManager")
    public DataSourceTransactionManager transactionManager(@Qualifier("sysRepoDataSource") DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @Primary
    @Bean(name = "systemRepoSqlSessionFactory")
    public SqlSessionFactory sqlSessionFactory(@Qualifier("sysRepoDataSource") DataSource dataSource) throws Exception {
        return new SessionFactoryBean(model, classPath).newInstance(dataSource);
    }

    @Bean("jdbcTemplateSystem")
    @Primary
    public JdbcTemplate jdbcTemplate(@Qualifier("sysRepoDataSource") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }
}
