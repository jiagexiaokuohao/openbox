package com.cq.common.config.constant;

/**
 * @author zhouqi
 */
public interface Constants {

    /**
     * 加密次数
     */
    int HASH_TIMES = 3;

    /**
     * 当前登陆的用户
     */
    String USER_SESSION_NAME = "user_session_name";
}
