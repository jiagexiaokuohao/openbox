package com.cq.service;

import com.cq.common.config.constant.Constants;
import com.cq.mapper.SysUserMapper;
import com.cq.model.SysUser;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zhouqi
 */
@Service
public class SysUserService {

    @Autowired
    private SysUserMapper sysUserMapper;


    public SysUser getByUsername(String username) {
        return sysUserMapper.findByUsername(username);
    }

    public String passwordEncoder(String credentials, String salt) {
        SimpleHash md5 = new SimpleHash("MD5", credentials, salt, Constants.HASH_TIMES);
        return md5.toString();
    }

    public Map<String, Object> register(SysUser sysUser) {
        Map<String, Object> map = new HashMap<>(3);
        String username = sysUser.getUsername();
        String password = sysUser.getPassword();
        if (username == null || password == null) {
            map.put("code", "501");
            map.put("msg", "信息不完整");
            return map;
        }

        password = passwordEncoder(password, username);
        sysUser.setPassword(password);
        sysUser.setCreateTime(new Date());
        sysUserMapper.create(sysUser);
        map.put("code", "200");
        return map;
    }
}
