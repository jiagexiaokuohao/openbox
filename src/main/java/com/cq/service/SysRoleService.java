package com.cq.service;

import com.cq.mapper.SysRoleMapper;
import com.cq.model.SysRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author zhouqi
 */
@Service
public class SysRoleService {

    @Autowired
    private SysRoleMapper sysRoleMapper;

    public SysRole getById(Integer id) {
        return sysRoleMapper.findById(id);
    }
}
