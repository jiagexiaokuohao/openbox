package com.cq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenboxApplication {

    public static void main(String[] args) {
        SpringApplication.run(OpenboxApplication.class, args);
    }
}
