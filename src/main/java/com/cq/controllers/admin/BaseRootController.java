package com.cq.controllers.admin;

import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author zhouqi
 */
@RequestMapping("/admin")
@Controller
public class BaseRootController {

    @RequiresRoles("admin")
    @RequestMapping("")
    public String index() {
        return "admin/index";
    }
}
