package com.cq.controllers;

import com.cq.model.SysUser;
import com.cq.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * @author zhouqi
 */
@RequestMapping("/user")
@Controller
public class SysUserController {

    @Autowired
    private SysUserService sysUserService;

    @ResponseBody
    @PostMapping("/add")
    public Map<String, Object> add(SysUser sysUser){
        return sysUserService.register(sysUser);
    }
}
