package com.cq.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author zhouqi
 */
@Controller
public class IndexController {


    @GetMapping("/404")
    public String notFound() {
        return "404";
    }

    @GetMapping("/403")
    public String unauthorized() {
        return "403";
    }
}
